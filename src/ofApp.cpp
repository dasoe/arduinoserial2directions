// =============================================================================
//
// Copyright (c) 2014-2016 Christopher Baker <http://christopherbaker.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// =============================================================================


#include "ofApp.h"


/*
This is an example of how to pass serial information from OF App to Arduino AND BACK in a RELIABLE way.
(This is for advanced tasks - for simple tasks Firmata (on the Arduino) and ofArduino object will be sufficient.)
This App relies on ofxSerial which relies on ofxIO (and for OF 0.10 ofxPoco).

// 1. Upload the ofCommunicationIntsBothSides.ino sketch (in this example's
//    Arduino/ folder) to an Arduino board.  This sketch requires
//    the Arduino PacketSerial library https://github.com/bakercp/PacketSerial.
// 2. When having problems, check the "listDevices" call below to make sure the correct serial
//    device is connected.

*/

void ofApp::setup()
{
	// get info on connected devices
	std::vector<ofx::IO::SerialDeviceInfo> devicesInfo = ofx::IO::SerialDeviceUtils::listDevices();
	ofLogNotice("ofApp::setup") << "Connected Devices: ";
	for (std::size_t i = 0; i < devicesInfo.size(); ++i)
	{
		ofLogNotice("ofApp::setup") << "\t" << devicesInfo[i];
	}
	// connect or give feedback in case problems emerge
	if (!devicesInfo.empty())
	{
		// Connect to the first matching device.
		
		bool success = device.setup(devicesInfo[0], 115200);
		if (success)
		{
			device.registerAllEvents(this);	
			ofLogNotice("ofApp::setup") << "Successfully setup " << devicesInfo[0];
		}
		else
		{
			ofLogNotice("ofApp::setup") << "Unable to setup " << devicesInfo[0];
		}
	}
	else
	{
		ofLogNotice("ofApp::setup") << "No devices connected.";
	}
}

void ofApp::exit()
{
	device.unregisterAllEvents(this);
}


void ofApp::update()
{
	// sending strings is direct and easy. PacketSerialDevice object we use is taking care of terminatino characters, errors and the like.
	// This example shows how to send ints (any value) and booleans (e.g button states)
	// both we transform to hex values for optimization (10001010 or 138 both is longer than 8A)
	// than we send it as string representation of the hex values.
	// In ths simple case we HARDCODE which value is inside of which characters here as well as in Arduino code:
	// first 2 characters represent first (int) value, 2 characters is second (int) value, 5th character represents hex value of 4 buttons
	// so we have to check that the total string sent is always 5 characters, otherwise it will not work
	// of course we could do this also with seperation characters. 


	// ######################## preparation HEX String ##############################
	// calculate numbers to send (here hardcoded as an example)
	int testNumber = 4;
	int testNumber2 = 125;

	// check state of buttons/switches (here hardcoded as an example)
	bool button1 = true;
	bool button2 = false;
	bool button3 = false;
	bool button4 = true;

	bitset<4> set(ofToString(button1) + ofToString(button2) + ofToString(button3) + ofToString(button4));

	// generate hexed string representation (24->18; 120->78; 1101->d), leading 0 if necessary
	std::stringstream hexNumber;
	hexNumber << setfill('0') << setw(2) << hex << testNumber << setfill('0') << setw(2) << hex << testNumber2 << hex << set.to_ulong();

	// just in case we wanted to create BINARY representation (which does not make sense in terms of optimization but anyway) it would be:
	// string binNumber = bitset<8>(testNumber2).to_string() + bitset<8>(testNumber).to_string();
	// ofx::IO::ByteBuffer buffer(binNumber);

	// ######################## Direct String ##############################
	// calculate numbers to send (here hardcoded as an example)
	int testNumber3 = 12;
	int testNumber4 = 495;

	// check state of buttons/switches (here hardcoded as an example)
	bool button5 = true;
	bool button6 = true;
	bool button7 = false;
	bool button8 = true;

	std::stringstream directNumber;
	directNumber << setfill('0') << setw(3) << testNumber3 << setfill('0') << setw(3) << testNumber4;

	// create the byte buffer
	ofx::IO::ByteBuffer buffer(hexNumber.str() + directNumber.str() + ofToString(button5) + ofToString(button6) + ofToString(button7) + ofToString(button8));
	// Send the byte buffer.
	// ofx::IO::PacketSerialDevice will encode the buffer, send it to the
	// receiver, and send a packet marker.
	device.send(buffer);
	ofLogNotice("string '" + hexNumber.str() + directNumber.str() + ofToString(button5) + ofToString(button6) + ofToString(button7) + ofToString(button8) + "' wurde gesendet");

}


void ofApp::draw()
{
	ofBackground(0);
	ofSetColor(255);

	// debug info
	std::stringstream ss;
	ss << "         FPS: " << ofGetFrameRate() << std::endl;
	ss << "Connected to: " << device.getPortName();
	ofDrawBitmapString(ss.str(), ofVec2f(20, 20));

	int x = 20;
	int y = 50;
	int height = 20;

	// draw the serial messsage the Arduino sent back
	ofSetColor(255);
	ofDrawBitmapString(serialMessage.message, ofVec2f(x, y));

	// in case there is an error, draw is toos
	if (!serialMessage.exception.empty())
	{
		ofSetColor(255, 0, 0);
		ofDrawBitmapString(serialMessage.exception, ofVec2f(x + height, y));
		y += height;
	}

}


void ofApp::onSerialBuffer(const ofx::IO::SerialBufferEventArgs& args)
{
	// Decoded serial packets will show up here.
	SerialMessage message(args.getBuffer().toString(), "", 255);
	serialMessage = message;
}


void ofApp::onSerialError(const ofx::IO::SerialBufferErrorEventArgs& args)
{
	// Errors and their corresponding buffer (if any) will show up here.
	SerialMessage message(args.getBuffer().toString(),
		args.getException().displayText(),
		500);

	serialMessage = message;
}
