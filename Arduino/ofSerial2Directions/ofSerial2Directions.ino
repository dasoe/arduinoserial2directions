// =============================================================================
//
// Copyright (c) 2012-2014 Christopher Baker <http://christopherbaker.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// =============================================================================


#include <PacketSerial.h>


// The PacketSerial object.
// It cleverly wraps one of the Serial objects.
// While it is still possible to use the Serial object
// directly, it is recommended that the user let the
// PacketSerial object manage all serial communication.
// Thus the user should not call Serial.write(), etc.
// Additionally the user should not use the serialEvent()
// callbacks.
PacketSerial serial;


void setup()
{
  // We must specify a packet handler method so that
  serial.setPacketHandler(&onPacket);
  serial.begin(115200);
}


void loop()
{
  // Do other things here.

  // The update() method attempts to read in
  // any incoming serial data and emits packets via
  // the user's onPacket(const uint8_t* buffer, size_t size)
  // method registered with the setPacketHandler() method.
  //
  // The update() method should be called at the end of the loop().
  serial.update();
}

// ###########################################
// ####### actually receiving Values #########
// ###########################################
// The buffer is delivered already decoded.
void onPacket(const uint8_t* buffer, size_t size)
{
  // Make a temporary buffer.
  uint8_t tmp[size];
  // Copy the packet into our temporary buffer.
  memcpy(tmp, buffer, size);


  // ###########################################
  // ####### changing received Values ##########
  // ###########################################

  // we get (hardcoded in OF Part) a 15 character string
  // first 2 characters represent first hexed (int) value, 2 characters is second hexed (int) value,
  // 5th character represents hex value of 4 buttons (4 booleans)
  // character 6-8 and 9-11 represent direct (decimal) int value each
  // character 12-15 represent direct binary representatino of booleans

  // ####### HEX Part of string #####
  // get first 2 digits (note that we need a char[3] for a 2 digit representation: one additional termination
  // character (which is added automatically, though)
  char number1AsString[3] = { char(tmp[0]), char(tmp[1]) };
  // char to int: we have our first value!
  int number1 = strtol (number1AsString, NULL, 16);

  // get next 2 digits
  // character (which is added automatically, though)
  char number2AsString[3] = { char(tmp[2]), char(tmp[3]) };
  // char to int: we have our second value!
  int number2 = strtol (number2AsString, NULL, 16);

  // get last digit
  char numberForBoolAsString[2] = { char(tmp[4]) };
  int tempNumberForBool = strtol (numberForBoolAsString, NULL, 16);
  // get bits from this number (here we can use easy Arduino functions).
  // bitRead starts at 0 for the least-significant (rightmost) bit
  bool button4 = bitRead(tempNumberForBool, 0);
  bool button3 = bitRead(tempNumberForBool, 1);
  bool button2 = bitRead(tempNumberForBool, 2);
  bool button1 = bitRead(tempNumberForBool, 3);

  // ####### DIRECT Part of string #####
  char number3AsString[4] = { char(tmp[5]), char(tmp[6]), char(tmp[7]) };
  int number3 = strtol (number3AsString, NULL, 10);

  char number4AsString[4] = { char(tmp[8]), char(tmp[9]), char(tmp[10]) };
  int number4 = strtol (number4AsString, NULL, 10);


  bool button[4];
  for (int i = 0; i < 4; i++) {
    button[i] = (char(tmp[11+i]) == '1');
  }

  // ###########################################
  // ###### preparing Values for sending #######
  // ###########################################

  // now we have our values. The following is doing everything backwards, and send
  // the (hopefully) same values back to OF in reversed order
  // to actually send something we can't use string, we need a char[] (which can be casted to uint8_t *,
  // which is what the send function expects)

  // ####### HEX Parts of string #####

  // Number to char  (note that we need a char[3] for a 2 digit representation: one additional termination
  // character (which is added automatically, though)
  char number1ToChar[3];
  // itoa does not add leading 0, so use sprintf
  // see http://www.cplusplus.com/reference/cstdio/sprintf/
  // for 2nd parameter "format" ("%02X"), see http://www.cplusplus.com/reference/cstdio/printf/
  //  itoa(number1,number1ToChar,16);
  sprintf (number1ToChar, "%02X", number1);

  char number2ToChar[3];
  sprintf (number2ToChar, "%02X", number2);

  // generate number from button booleans
  // again we can use Arduino functions to make it easier
  char numberForBoolToChar[2];
  int tempNumberForBool_new;
  bitWrite(tempNumberForBool_new, 0, button4);
  bitWrite(tempNumberForBool_new, 1, button3);
  bitWrite(tempNumberForBool_new, 2, button2);
  bitWrite(tempNumberForBool_new, 3, button1);
  sprintf (numberForBoolToChar, "%X", tempNumberForBool_new);

  // ####### DIRECT Pars of string #####

  char number3ToChar[4];
  sprintf (number3ToChar, "%03u", number3);
  char number4ToChar[4];
  sprintf (number4ToChar, "%03u", number4);

  char buttonsChar[5] = {0};
  for (int i = 0; i < 4; i++) {
    if (button[i]) {
      buttonsChar[i] = '1';
    } else {
      buttonsChar[i] = '0';
    }
  }

  // ############## glueing together the char strings in reverse order ############
  // we also add hyphens when sending the values back - just for fun
  char stringTosendBack[21] = {0};
  strcat(stringTosendBack, buttonsChar);
  strcat(stringTosendBack, "-");
  strcat(stringTosendBack, number4ToChar); 
  strcat(stringTosendBack, "-");
  strcat(stringTosendBack, number3ToChar);  
  strcat(stringTosendBack, "-");
  strcat(stringTosendBack, numberForBoolToChar);
  strcat(stringTosendBack, "-");
  strcat(stringTosendBack, number2ToChar);
  strcat(stringTosendBack, "-");
  strcat(stringTosendBack, number1ToChar);



  // ###########################################
  // ######## actually sending Values ##########
  // ###########################################

  // Send the reversed buffer back.
  // The send() method will encode the buffer
  // as a packet, set packet markers, etc.
  // we have 55 digits to send plus the 5 hyphens, so 20
  serial.send((uint8_t *)stringTosendBack, 20);
}


